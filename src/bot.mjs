import {bot} from './services/bot.mjs';
import { Client, GatewayIntentBits, Partials } from 'discord.js';
import {TaskRepository} from 'bot-commons-tasks/src/services/task.mjs';
import {TaskListRepository} from 'bot-commons-tasks/src/services/taskList.mjs';
import path from 'node:path';
import {
  registerCommands,
  registerEvents,
  registerScripts,
  registerCronJobs,
  firstSteps,
  loginBot,
  setupGracefulShutdown
} from 'bot-commons-utils/src/utils/botSetup.mjs';

// Define the bot's root directory
bot.botRoot = path.resolve('.');

// Initialize Brain, check environment variables, etc
await firstSteps(bot);

// Create a new client instance
bot.client = new Client({
  partials: [Partials.Message, Partials.Channel, Partials.Reaction],
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildMembers,
    GatewayIntentBits.GuildMessageReactions,
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.DirectMessages,
  ],
});

// Register all commands in the folder
await registerCommands(bot);
// Register events
await registerEvents(bot);
// Register scripts in the folder
await registerScripts(bot);
// Register all cron jobs in the folder
await registerCronJobs(bot);

// Log bot into Discord, set Guild in Bot service
await loginBot(bot);

bot.taskLists = new TaskListRepository(bot);
bot.tasks = new TaskRepository();

// Mostly needed for Reaction Role manager (which this bot doesn't have)
// Stop the bot when the process is closed (via Ctrl-C).
setupGracefulShutdown(bot);
