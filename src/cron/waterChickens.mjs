import { ensureBrainKeyExistsPromise } from 'bot-commons-utils/src/utils/brain.mjs';

const channel_id = process.env.PRIVATE_ALERTS_CHANNEL_ID;

let cooldown = false;

export default {
  schedule: '1,31 7-19 * * *', // Every 30 minutes between 7am and 7pm

  task: async function (bot) {
    if (cooldown) {
      return;
    }

    try {
      let weather = await ensureBrainKeyExistsPromise('weather');

      if (!weather.data || !weather.timestamp) {
        console.error("No weather data found in brain.");
        return;
      }

      const lastFetchedTimestamp = weather.timestamp;
      const lastFetchedDate = new Date(lastFetchedTimestamp);
      const currentTime = Date.now();

      if ((currentTime - lastFetchedTimestamp) > 60 * 60 * 1000) {
        console.log("Weather data is older than 60 minutes, skipping this round.");
        return;
      }

      const lastFetched = lastFetchedDate.toLocaleString();

      if (weather.data.main.temp > 90) {
        const guild = bot.guild;
        const channel = guild.channels.cache.get(channel_id);
        if (channel) {
          const actualTemp = weather.data.main.temp;
          await channel.send(`The temperature is ${actualTemp}F. You should water the chickens. \n*(Last checked: ${lastFetched})*`);
        } else {
          console.error("Channel not found");
        }

        // Set cooldown
        cooldown = true;
        setTimeout(() => cooldown = false, 4 * 60 * 60 * 1000); // 4 hours
      }
    } catch (error) {
      console.error("Error accessing weather data from brain:", error);
    }
  },
};
