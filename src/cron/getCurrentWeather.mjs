import fetch from 'node-fetch';
import {brain, ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';

const weather_api_key = process.env.OPENWEATHERMAP_API_KEY;
const city_id = process.env.CITY_ID;

export default {
  schedule: '0,30 * * * *', // Every 30 minutes

  task: async function () {
    try {
      const response = await fetch(`https://api.openweathermap.org/data/2.5/weather?id=${city_id}&units=imperial&appid=${weather_api_key}`);
      const weatherData = await response.json();

      let weather = await ensureBrainKeyExistsPromise('weather');
      weather.data = weatherData;
      weather.timestamp = Date.now();
      await brain.write();
    } catch (error) {
      console.error("Error fetching weather data:", error);
    }
  },
};
