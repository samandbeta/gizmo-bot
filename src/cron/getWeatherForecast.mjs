import fetch from 'node-fetch';
import { brain, ensureBrainKeyExistsPromise } from 'bot-commons-utils/src/utils/brain.mjs';

const weather_api_key = process.env.OPENWEATHERMAP_API_KEY;
const city_id = process.env.CITY_ID;

export default {
  schedule: '0 7 * * *', // Every day at 7 am

  task: async function () {
    try {
      const url = `https://api.openweathermap.org/data/2.5/forecast?id=${city_id}&units=imperial&appid=${weather_api_key}`;
      const response = await fetch(url);
      const forecastData = await response.json();

      let forecast = await ensureBrainKeyExistsPromise('daily_forecast');
      forecast.city = forecastData.city
      forecast.data = forecastData.list;
      forecast.timestamp = Date.now();
      await brain.write();
    } catch (error) {
      console.error("Error fetching forecast data:", error);
    }
  },
};
