import {Client, Guild} from 'discord.js';
import {TaskListRepository} from 'bot-commons-tasks/src/services/taskList.mjs';
import {TaskRepository} from 'bot-commons-tasks/src/services/task.mjs';

export class Bot {
  /** @var {Client<true>} */
  client;

  /** @var {Guild} */
  guild;

  /** @var {string} */
  botRoot;

  /** @var {TaskListRepository} */
  taskLists;

  /** @var {TaskRepository} */
  tasks;
}

/** Container for accessing global services. */
export const bot = new Bot();
