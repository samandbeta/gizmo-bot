import {handleCommandInteraction} from 'bot-commons-utils/src/utils/interactionCreateUtil.mjs';

const interactionCreate = {
  name: 'interactionCreate',
  async execute(interaction, bot) {
    // console.log(`${interaction.user.tag} in #${interaction.channel.name} triggered an interaction.`);
    await handleCommandInteraction(interaction, bot);
  },
};


export default interactionCreate;
